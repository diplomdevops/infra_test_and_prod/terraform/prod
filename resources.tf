# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "${var.name_dynamic_security_group}-${var.env}"

  dynamic "ingress" {
    # Зададим правило, по каким портам можно обращаться к нашим серверам
    for_each = var.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = var.cidr_blocks_ingress
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = var.cidr_blocks_egress
  }

  tags = {
    Name        = var.tag_name_aws_security_group
    Environment = var.env
  }
}


# Создаём Launch Configuration - это сущность, которая определяет конфигурацию запускаемых серверов. Размер, ,
resource "aws_launch_configuration" "web" {
  name_prefix = "${var.name_prefix_launch_configuration}-${var.env}"
  # какой будет использоваться образ
  image_id = data.aws_ami.ubuntu.id
  # Размер машины (CPU и память)
  instance_type = var.instance_type
  # какие права доступа
  security_groups = [aws_security_group.web.id]
  # какие следует запустить скрипты при создании сервера
  user_data = file("user_data.sh")
  # какой SSH ключ будет использоваться
  key_name = var.ssh_key_name
  # Если мы решим обновить инстанс, то, прежде, чем удалится старый инстанс, который больше не нужен, должен запуститься новый
  lifecycle {
    create_before_destroy = true
  }
}

# AWS Autoscaling Group для указания, сколько нам понадобится инстансов
resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}-${var.env}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = var.min_size_instance
  max_size             = var.max_size_instance
  min_elb_capacity     = var.min_elb_capacity
  health_check_type    = var.health_check_type
  # и в каких подсетях, каких Дата центрах их следует разместить
  vpc_zone_identifier = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  # Ссылка на балансировщик нагрузки, который следует использовать
  load_balancers = [aws_elb.web.name]
  dynamic "tag" {
    for_each = {
      Name        = "${var.name_autoscaling_group}-${var.env}"
      Environment = var.env
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Elastic Load Balancer проксирует трафик на наши сервера
resource "aws_elb" "web" {
  name = "${var.name_elastic_load_balancer}-${var.env}"
  # перенаправляет трафик на несколько Дата центров
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  # слушает на порту 80
  listener {
    lb_port           = var.lb_port
    lb_protocol       = "http"
    instance_port     = var.instance_port
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = var.healthy_threshold
    unhealthy_threshold = var.unhealthy_threshold
    timeout             = var.timeout
    target              = "HTTP:80/"
    interval            = var.interval
  }
  tags = {
    Name        = var.name_elastic_load_balancer
    Environment = var.env
  }
}

# Созаём подсети в разных Дата центрах
resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}
